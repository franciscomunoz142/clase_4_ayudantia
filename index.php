<?php 
    include("Modulo/EmpleadosBackend.php");
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Datos de empleados</title>

    <!--Bootstrap-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    
    <!--FontAwesome-->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

    <!--Nesesario para DatePicker (calendario en formulario)-->
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="container" style="margin-top: 80px; margin-left: 40px;">
        <h2>Lista de los empleados</h2>
        <hr>
        <!--Filtros-->
        <div class="row">

            <div class="col-3">
                <select class="form-control">
                    <option value="0">Filtro de datos de empleado</option>
                    <option value="1">Fijo</option>
                    <option value="2" selected>Contratado</option>
                    <option value="3">Outsourcing</option>
                </select>
            </div>
            
            <div class="col-3">
                <button class="btn btn-primary form-control" data-toggle="modal" data-target="#crearNuevoEmpleado">
                    Crear un nuevo empleado
                </button>
            </div>
        </div>
        <br>
        <!--Tabla de empleados-->
        <div class="table-responsive">
            <table class="table table-striped">
                <!--Table Row-->
                <tr>
                    <!--Celda--> 
                    <th>N°</th>
                    <th>Codigo</th>
                    <th>Nombre</th>
                    <th>Lugar de nacimiento</th>
                    <th>Fecha de nacimiento</th>
                    <th>Telefono</th>
                    <th>Cargo</th>
                    <th>Estados</th>
                    <th>Acciones</th>
                </tr>

                <?php
                    $contador = 1;
                    if($ConsultaEmpleados): foreach($ConsultaEmpleados as $row):
                ?>

                <tr>
                    <td><?php echo $contador?></td>
                    <td><?php echo $row['codigo']; ?></td>
                    <td><?php echo $row['nombres']; ?></td>
                    <td><?php echo $row['lugar_nacimiento']; ?></td>
                    <td><?php echo $row['fecha_nacimiento']; ?></td>
                    <td><?php echo $row['telefono']; ?></td>
                    <td><?php echo $row['puesto']; ?></td>
                    <td>
                        <?php 
                            if($row['estado'] == '1'){
                                echo '<span class="badge badge-success">Fijo</span>';
                            }else if($row['estado'] == '2'){
                                echo '<span class="badge badge-info">Contratado</span>';
                            }else if($row['estado'] == '3'){
                                echo '<span class="badge badge-warning">Outsoursing</span>';
                            }
                        ?>
                    </td>

                    <td>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#editarEmpleado"><i class="fas fa-edit"></i></button>
                        <button class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                    </td>

                    <?php $contador++; ?>
                </tr>

                <?php endforeach; endif ?>
                
            </table>
        </div>
    </div>

    <!-- Modal (Crear Nuevo Empleado) -->
    <div class="modal fade" id="crearNuevoEmpleado" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Crear Nuevo Empleado</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="Modulo/CrearEmpleado.php" method="POST">

                        <div class="row">
                            <div class="form-group col-3">
                                <label>Codigo</label>
                                <input type="text" class="form-control" name="codigo" required>  
                            </div>
                            
                            <div class="form-group col-9">
                                <label>Nombre</label>
                                <input type="text" class="form-control" name="nombre" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-6">
                                <label>Lugar de nacimiento</label>
                                <input type="text" class="form-control" name="lugar-nacimiento" required>  
                            </div>
                            
                            <div class="form-group col-6">
                                <label>Fecha de nacimiento</label>
                                <!--id=datepicker para llamar a script-->
                                <input type="text" class="form-control" id="datepicker" name="fecha-nacimiento" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-6">
                                <label>Telefono</label>
                                <input type="text" class="form-control" name="telefono" required>  
                            </div>
                            
                            <div class="form-group col-6">
                                <label>Cargo</label>
                                <input type="text" class="form-control" name="cargo" required>
                            </div>
                        </div>

                        <div>
                            <div class="form-group">
                                <label>Estados</label>
                                <select class="form-control" name="estado" required>
                                    <option value="">Filtro de datos de empleado</option>
                                    <option value="1">Fijo</option>
                                    <option value="2">Contratado</option>
                                    <option value="3">Outsourcing</option>
                                </select>  
                            </div>
                        </div>

                        <div class="offset-10">
                            <button type="submit" class="btn btn-primary">Enviar</button>
                        </div>
                    </form>
                </div>
                
            </div>
        </div>
    </div>

    <!--Modal (Editar Empleado)-->
    <div class="modal fade" id="editarEmpleado" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Editar Empleado</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>

                        <div class="row">
                            <div class="form-group col-3">
                                <label>Codigo</label>
                                <input type="text" class="form-control" required>  
                            </div>
                            
                            <div class="form-group col-9">
                                <label>Nombre</label>
                                <input type="text" class="form-control" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-6">
                                <label>Lugar de nacimiento</label>
                                <input type="text" class="form-control" required>  
                            </div>
                            
                            <div class="form-group col-6">
                                <label>Fecha de nacimiento</label>
                                <input type="text" class="form-control" id="datepicker2" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-6">
                                <label>Telefono</label>
                                <input type="text" class="form-control" required>  
                            </div>
                            
                            <div class="form-group col-6">
                                <label>Cargo</label>
                                <input type="text" class="form-control" required>
                            </div>
                        </div>

                        <div>
                            <div class="form-group">
                                <label>Estados</label>
                                <select class="form-control" required>
                                    <option value="">Filtro de datos de empleado</option>
                                    <option value="1">Fijo</option>
                                    <option value="2">Contratado</option>
                                    <option value="3">Outsourcing</option>
                                </select>  
                            </div>
                        </div>

                        <div class="offset-10">
                            <button type="submit" class="btn btn-primary">Enviar</button>
                        </div>
                    </form>
                </div>
                
            </div>
        </div>
    </div>


    <!--JQuery-->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <!--Bootstrap-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <!--Para DatePicker (Calendario en formulario)-->
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script>
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>
    <script>
        $('#datepicker2').datepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>
</body>
</html>
